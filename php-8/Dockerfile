FROM php:8-fpm

ARG WWW_DATA_GID
ARG WWW_DATA_UID

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apt-get update && apt-get -y upgrade && apt-get -y install git bash bash-doc bash-completion unzip vim \
    && chmod uga+x /usr/local/bin/install-php-extensions && sync
RUN install-php-extensions pdo_mysql
RUN install-php-extensions mysqli
RUN install-php-extensions bcmath
RUN IPE_GD_WITHOUTAVIF=1 install-php-extensions gd
RUN install-php-extensions imagick
RUN install-php-extensions zip
RUN install-php-extensions intl
RUN install-php-extensions exif
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN groupmod -g ${WWW_DATA_GID} www-data && usermod -u ${WWW_DATA_UID} www-data

RUN mkdir /home/www-data
RUN chown www-data:www-data /home/www-data
RUN usermod -d /home/www-data www-data

RUN sed -i -e 's/expose_php = On/expose_php = Off/' "$PHP_INI_DIR/php.ini"
RUN sed -i -e 's/post_max_size = 8M/post_max_size = 20M/' "$PHP_INI_DIR/php.ini"
RUN sed -i -e 's/upload_max_filesize = 2M/upload_max_filesize = 10M/' "$PHP_INI_DIR/php.ini"
RUN sed -i -e 's/memory_limit = 128M/memory_limit = 256M/' "$PHP_INI_DIR/php.ini"
RUN sed -i -e 's/max_execution_time = 30/max_execution_time = 60/' "$PHP_INI_DIR/php.ini"

COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer self-update

RUN mkdir /usr/local/nvm
ENV NVM_DIR /usr/local/nvm
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install lts/*
RUN cp ~/.bashrc /home/www-data/
RUN . $NVM_DIR/nvm.sh \
    && export NODE_PATH="$NVM_DIR/versions/node/$(nvm current)/bin" \
    && cd /usr/bin \
    && ln -s "$NODE_PATH/node" \
    && ln -s "$NODE_PATH/npm" \
    && ln -s "$NODE_PATH/npx"
